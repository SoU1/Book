package com.asmaa.newlogin;

/**
 * Created by dell on 4/18/2018.
 */

public class User {
    private String Bookname;
    private String Authorname;
     int img;

    public User(String Bookname, String Authorname , int img) {
        this.Bookname = Bookname;
        this.Authorname = Authorname;
        this.img = img;

    }

    public String getBookname() {
        return Bookname;
    }

    public void setBookname(String Bookname) {
        this.Bookname = Bookname;
    }

    public String getAuthorname() {
        return Authorname;
    }

    public void setAuthorname(String Authorname) {
        this.Authorname = Authorname;
    }


}
