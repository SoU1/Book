package com.asmaa.newlogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.content.Intent;



public class page6 extends AppCompatActivity {

    Button b1;

    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page6);


        //button
        b1 = (Button)findViewById(R.id.butt);

        img = (ImageView)findViewById(R.id.imageView);

    }

    public void btn_camera(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,100);
    }

    @Override
    protected void onActivityResult(int requestCode , int resultCode , Intent data){
        if (requestCode==100 && resultCode==RESULT_OK){
            img.setImageBitmap((Bitmap)data.getExtras().get("data"));
        }
    }

    public void bttn_next(View v) {
        Intent intent = new Intent(page6.this, page3.class);
        startActivity(intent);
    }


}
