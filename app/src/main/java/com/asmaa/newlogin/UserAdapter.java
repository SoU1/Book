package com.asmaa.newlogin;

import android.widget.TextView;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.view.View.OnClickListener;




/**
 * Created by dell on 4/18/2018.
 */

public class UserAdapter extends ArrayAdapter <User>{
    Context context;
    int resource;
    public UserAdapter(@NonNull Context context , @LayoutRes int resource , @NonNull ArrayList<User> Objects) {

        super (context , resource , Objects );
        this.context = context;
        this.resource = resource;

    }

    @NonNull
    @Override
    public View getView(int position ,@NonNull View convertView , @NonNull ViewGroup parent ){
        convertView = LayoutInflater.from(context).inflate(resource,parent,false);
        TextView bookname = (TextView)convertView.findViewById(R.id.bookname);
        TextView authorname = (TextView)convertView.findViewById(R.id.authorname);
        ImageView image = (ImageView)convertView.findViewById(R.id.image);
        User currentUser = getItem(position);
        bookname.setText(currentUser.getBookname());
        authorname.setText(String.valueOf(currentUser.getAuthorname()));
        image.setImageResource(currentUser.img);
        return convertView;


    }
}
